from odoo import _, api, fields, models


class CustomerReview(models.Model):
    _name = "customer.review"

    product_id = fields.Many2one('product.template' )
    customer_id = fields.Many2one("res.users", string='Review By')
    name = fields.Text(string='Comment')
    rating = fields.Integer(string='Rating')
    date = fields.Datetime(string='Created On')
    email = fields.Char(string='Email' )
