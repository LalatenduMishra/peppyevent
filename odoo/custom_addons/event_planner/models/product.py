from odoo import _, api, fields, models


class ProductAsService(models.Model):
    _inherit = 'product.template'

    # category = fields.Selection([('wedding','Wedding'),
    #                              ('meeting','Bussiness Meeting'),
    #                              ('party','Parties'),
    #                              ('ceremony','Ceremonies'),
    #                              ('festival','Festivals')], string="Category")

    location = fields.Many2many('res.country.state', string="City/Location", domain="[('country_id.code','=','IN')]")
    user_reviews = fields.One2many('customer.review','product_id',string="User Reviews")
    average_rating = fields.Selection([(0,0),(1,1),(2,2),(3,3),(4,4),(5,5)], string="Average Rating")


