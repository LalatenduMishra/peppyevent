# -*- coding: utf-8 -*-
{
    'name': 'Event Planner',
    'category': 'Event',
    'description': """
Event Planner
=============================
""",
    'version': '1.0',
    'depends': ['product','event' ],
    'data': [
        'views/review_rating_view.xml',
        'views/product_view.xml',
    ],
    'application' : True,
    'installable': True,
}
